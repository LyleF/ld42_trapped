﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    private static StageManager instance;
    public static StageManager Instance
    {
        get
        {
            return instance;
        }
    }
    [SerializeField]
    private GameObject Overlay;

    [SerializeField]
    Transform parentReference;
    [SerializeField]
    private GameObject nodePrefab;

    private List<GameObject> nodeSpaces;
    private List<int> takenIndecesX;
    private List<int> takenIndecesY;
    private NodeController playerController;
    public bool win;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        this.Setup();
    }

    public List<GameObject> GetNodes
    {
        get { return this.nodeSpaces; }
    }

    public void Win()
    {
        Overlay.SetActive(true);
        win = true;
    }

    public void Setup()
    {
        win = false;
        Overlay.SetActive(false);
        ClearSpace();
        StartCoroutine(SetupLevel());
    }
    private IEnumerator SetupLevel()
    {
        nodeSpaces = new List<GameObject>();
        takenIndecesX = new List<int>();
        takenIndecesY = new List<int>();
        Vector3 initialPosition = new Vector3(50, -50);

        for (int i = 0; i < 4; i++)
        {
            GameObject nodeY = GameObject.Instantiate(nodePrefab);
            nodeY.transform.SetParent(this.parentReference);
            RectTransform rectY = nodeY.GetComponent<RectTransform>();

            initialPosition.y = -50 - (100 * i);
            rectY.anchoredPosition = new Vector2(50, initialPosition.y);

            this.nodeSpaces.Add(nodeY);

            for (int j = 0; j < 6; j++)
            {
                initialPosition.x = 150 + (100 * j);

                GameObject nodeX = GameObject.Instantiate(nodePrefab);
                nodeX.transform.SetParent(this.parentReference);
                RectTransform rectX = nodeX.GetComponent<RectTransform>();

                rectX.anchoredPosition = initialPosition;

                NodeController nodeController = nodeX.GetComponent<NodeController>();

                this.nodeSpaces.Add(nodeX);
            }
        }

        int randomNumberY = 0;
        int randomNumberX = 0;

        bool playerReady, X1r, X2r, X3r;

        while (takenIndecesX.Count != 3)
        {
            randomNumberX = Random.Range(0, 28);

            if (takenIndecesX.Contains(randomNumberX)) continue;

            takenIndecesX.Add(randomNumberX);
        }

        for (int i = 0; i < this.nodeSpaces.Count; i++)
        {
            NodeController nodeController = this.nodeSpaces[i].GetComponent<NodeController>();
            if (takenIndecesX.Contains(i) && i != 0)
            {
                nodeController.SetNodeType(new NodeBlockade());
            }

            if (i == 0)
                nodeController.SetNodeType(new NodePlayer());

            if (i == 27)
                nodeController.SetNodeType(new NodeExit());
        }

        yield return null;
    }

    private void ClearSpace()
    {
        if (nodeSpaces == null)
            return;

        foreach (GameObject g in nodeSpaces)
        {
            Destroy(g);
        }
    }
}
