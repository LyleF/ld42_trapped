﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeView : MonoBehaviour
{
    [SerializeField]
    private NodeController controller;

    [SerializeField]
    private Sprite playerSprite;
    [SerializeField]
    private Sprite occupiedSprite;
    [SerializeField]
    private Sprite exitSprite;

    [SerializeField]
    private Image image;

    private void Awake()
    {
        this.controller.NodeChange += OnNodeChange;
    }

    private void OnDestroy()
    {
        this.controller.NodeChange -= OnNodeChange;
    }

    private void OnNodeChange(Node node)
    {
        if (node is NodeBlockade)
            this.image.sprite = occupiedSprite;
        else
            if (node is NodeExit)
            this.image.sprite = exitSprite;
        else
            if (node is NodePlayer)
            this.image.sprite = playerSprite;
        else
            this.image.sprite = null;
    }
}
