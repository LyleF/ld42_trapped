﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodePlayer : Node
{
    public int moveCounter
    {
        get;
        private set;
    }
    List<GameObject> nodeSpaces = StageManager.Instance.GetNodes;

    public override void Update()
    {
        if (StageManager.Instance.win)
            return;

        foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(key))
            {
                this.CheckDirection(key);
            }
        }
    }

    private void CheckDirection(KeyCode key)
    {
        switch (key)
        {
            case KeyCode.UpArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    if (nodeSpaces[i].GetComponent<NodeController>().GetNodeType is NodePlayer)
                    {
                        if ((i - 7) < 0 || nodeSpaces[i - 7].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            return;
                        }

                        //pos swap

                        if (nodeSpaces[i - 7].GetComponent<NodeController>().GetNodeType is NodeExit == false)
                        {
                            Vector3 tempPos = nodeSpaces[i - 7].transform.position;
                            nodeSpaces[i - 7].transform.position = nodeSpaces[i].transform.position;
                            nodeSpaces[i].transform.position = tempPos;

                            GameObject tempNode = nodeSpaces[i - 7];
                            nodeSpaces[i - 7] = nodeSpaces[i];
                            nodeSpaces[i] = tempNode;

                            break;
                        }
                        else
                            StageManager.Instance.Win();
                    }
                }
                this.moveCounter++;

                if(this.moveCounter == 2)
                {
                    CheckNextSpace(key);
                }
                break;

            case KeyCode.DownArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    if (nodeSpaces[i].GetComponent<NodeController>().GetNodeType is NodePlayer)
                    {
                        if ((i + 7) >= nodeSpaces.Count || nodeSpaces[i + 7].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            return;
                        }

                        //pos swap
                        if (nodeSpaces[i + 7].GetComponent<NodeController>().GetNodeType is NodeExit == false)
                        {
                            Vector3 tempPos = nodeSpaces[i + 7].transform.position;
                            nodeSpaces[i + 7].transform.position = nodeSpaces[i].transform.position;
                            nodeSpaces[i].transform.position = tempPos;

                            GameObject tempNode = nodeSpaces[i + 7];
                            nodeSpaces[i + 7] = nodeSpaces[i];
                            nodeSpaces[i] = tempNode;

                            break;
                        }
                        else
                            StageManager.Instance.Win();
                    }
                }
                this.moveCounter++;

                if (this.moveCounter == 2)
                {
                    CheckNextSpace(key);
                }
                break;

            case KeyCode.RightArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    if (nodeSpaces[i].GetComponent<NodeController>().GetNodeType is NodePlayer)
                    {
                        if ((i + 1) % 7 == 0 || nodeSpaces[i + 1].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            return;
                        }

                        //pos swap

                        if (nodeSpaces[i + 1].GetComponent<NodeController>().GetNodeType is NodeExit == false)
                        {
                            Vector3 tempPos = nodeSpaces[i + 1].transform.position;
                            nodeSpaces[i + 1].transform.position = nodeSpaces[i].transform.position;
                            nodeSpaces[i].transform.position = tempPos;

                            GameObject tempNode = nodeSpaces[i + 1];
                            nodeSpaces[i + 1] = nodeSpaces[i];
                            nodeSpaces[i] = tempNode;

                            break;
                        }
                        else
                            StageManager.Instance.Win();
                    }
                }
                this.moveCounter++;

                if (this.moveCounter == 2)
                {
                    CheckNextSpace(key);
                }
                break;

            case KeyCode.LeftArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    if (nodeSpaces[i].GetComponent<NodeController>().GetNodeType is NodePlayer)
                    {
                        if (i % 7 == 0 || i == 0 || nodeSpaces[i - 1].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            return;
                        }

                        //pos swap

                        if (nodeSpaces[i - 1].GetComponent<NodeController>().GetNodeType is NodeExit == false)
                        {
                            Vector3 tempPos = nodeSpaces[i - 1].transform.position;
                            nodeSpaces[i - 1].transform.position = nodeSpaces[i].transform.position;
                            nodeSpaces[i].transform.position = tempPos;

                            GameObject tempNode = nodeSpaces[i - 1];
                            nodeSpaces[i - 1] = nodeSpaces[i];
                            nodeSpaces[i] = tempNode;

                            break;
                        }
                        else
                            StageManager.Instance.Win();
                    }
                }
                this.moveCounter++;

                if (this.moveCounter == 2)
                {
                    CheckNextSpace(key);
                }
                break;
            case KeyCode.Space:
                StageManager.Instance.Setup();
                break;
        }
    }

    private void CheckNextSpace(KeyCode lastKeyPressed)
    {
        this.moveCounter = 0;

        switch (lastKeyPressed)
        {
            case KeyCode.UpArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    NodeController controller = nodeSpaces[i].GetComponent<NodeController>();

                    if (controller.GetNodeType is NodePlayer)
                    {
                        if ((i - 7) < 0 || nodeSpaces[i - 7].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            nodeSpaces[i + 7].GetComponent<NodeController>().SetNodeType(new NodeBlockade());
                            return;
                        }

                        nodeSpaces[i - 7].GetComponent<NodeController>().SetNodeType(new NodeBlockade());

                        break;
                    }
                }
                break;

            case KeyCode.DownArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    NodeController controller = nodeSpaces[i].GetComponent<NodeController>();

                    if (controller.GetNodeType is NodePlayer)
                    {
                        if ((i + 7) >= nodeSpaces.Count || nodeSpaces[i + 7].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            nodeSpaces[i - 7].GetComponent<NodeController>().SetNodeType(new NodeBlockade());
                            return;
                        }

                        nodeSpaces[i + 7].GetComponent<NodeController>().SetNodeType(new NodeBlockade());

                        break;
                    }
                }
                break;

            case KeyCode.RightArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    NodeController controller = nodeSpaces[i].GetComponent<NodeController>();

                    if (controller.GetNodeType is NodePlayer)
                    {
                        if ((i + 1) % 7 == 0 || nodeSpaces[i + 1].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            nodeSpaces[i - 1].GetComponent<NodeController>().SetNodeType(new NodeBlockade());
                            return;
                        }

                        nodeSpaces[i + 1].GetComponent<NodeController>().SetNodeType(new NodeBlockade());

                        break;
                    }
                }
                break;

            case KeyCode.LeftArrow:
                for (int i = 0; i < nodeSpaces.Count; i++)
                {
                    NodeController controller = nodeSpaces[i].GetComponent<NodeController>();

                    if (controller.GetNodeType is NodePlayer)
                    {
                        if ((i) % 7 == 0 || i == 0 || nodeSpaces[i - 1].GetComponent<NodeController>().GetNodeType is NodeBlockade)
                        {
                            nodeSpaces[i + 1].GetComponent<NodeController>().SetNodeType(new NodeBlockade());
                            return;
                        }

                        nodeSpaces[i - 1].GetComponent<NodeController>().SetNodeType(new NodeBlockade());

                        break;
                    }
                }
                break;
        }
    }
}
