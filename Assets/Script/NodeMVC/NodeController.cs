﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class NodeController : MonoBehaviour
{
    [SerializeField]
    private NodeView nodeView;
    private NodeModel nodeModel;

    public event Action<Node> NodeChange;
    public event Action UpdateEvent;

    public void SetNodeType(Node node)
    {
        if(this.NodeChange != null)
        {
            this.NodeChange(node);
        }
    }

    public Node GetNodeType
    {
        get { return this.nodeModel.currentNodeType; }
    }

    private void Awake()
    {
        this.nodeModel = new NodeModel(this);
    }

    private void Update()
    {
        this.nodeModel.currentNodeType.Update();
    }
}

public class NodeModel
{
    public NodeModel(NodeController controller)
    {
        controller.NodeChange += this.UpdateCurrentNode;

    }

    public Node currentNodeType = new NodeFree();

    public void UpdateCurrentNode(Node node)
    {
        this.currentNodeType = node;
    }
}     
